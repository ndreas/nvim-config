default: update

update:
	nvim --headless "+Lazy! restore" +qa

update-deps:
	nvim --headless "+Lazy! update" +qa
	git add lazy-lock.json
	git commit -m "lazy-lock.json: Update $(git diff --cached --numstat lazy-lock.json | awk '{print $1 "\n" $2}' |sort |tail -n1) packages"
