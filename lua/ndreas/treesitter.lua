require("nvim-treesitter.configs").setup({
  highlight = { enable = true },
  incremental_selection = { enable = true },
  textobjects = {
    select = {
      enable = true,
      lookahead = true,
      keymaps = { ["af"] = "@function.outer", ["if"] = "@function.inner" },
    },
    move = { enable = true, goto_previous_start = { ["åm"] = "@function.outer" } },
    swap = {
      enable = true,
      swap_previous = { ["<left>"] = "@parameter.inner" },
      swap_next = { ["<right>"] = "@parameter.inner" },
    },
  },
  textsubjects = {
    enable = true,
    keymaps = {
      ["."] = "textsubjects-smart",
      [";"] = "textsubjects-container-outer",
    },
  },
  endwise = { enable = true },
  ensure_installed = {
    "bash",
    "comment",
    "css",
    "csv",
    "diff",
    "dockerfile",
    "fennel",
    "fish", -- needed by nix
    "git_config",
    "git_rebase",
    "gitattributes",
    "gitcommit",
    "gitignore",
    "go",
    "gomod",
    "gosum",
    "haskell", -- needed by nix
    "html",
    "javascript",
    "jsdoc",
    "json",
    "just",
    "lua",
    "make",
    "markdown",
    "markdown_inline",
    "nix",
    "printf",
    "python",
    "query",
    "regex",
    "rust",
    "scss",
    "sql",
    "ssh_config",
    "tmux",
    "toml",
    "tsv",
    "tsx",
    "typescript",
    "udev",
    "vim",
    "vimdoc",
    "xml",
    "yaml",
  },
})

local treesj = require("treesj")
treesj.setup({
  use_default_keymaps = false,
  max_join_length = 100000,
})
vim.keymap.set("n", "<leader>t", treesj.toggle)
vim.keymap.set("n", "<leader>T", function()
  treesj.toggle({ split = { recursive = true } })
end)
