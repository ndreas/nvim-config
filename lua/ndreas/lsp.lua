local vim = vim
local api = vim.api
local augroup = vim.api.nvim_create_augroup
local autocmd = vim.api.nvim_create_autocmd
local lspconfig = require("lspconfig")
local lspconfigutil = require("lspconfig/util")
local lspformat = require("lsp-format")

-- ~/.local/state/nvim/lsp.log
vim.lsp.set_log_level("OFF")

local home = vim.uv.os_homedir()

vim.api.nvim_create_autocmd("LspAttach", {
  callback = function(args)
    local bufnr = args.buf
    local client = vim.lsp.get_client_by_id(args.data.client_id)
    if client == nil then
      return
    end

    if client.config.flags then
      client.config.flags.allow_incremental_sync = true
      client.config.flags.debounce_text_changes = 250
    end

    local function bind(lhs, rhs, mode)
      api.nvim_buf_set_keymap(bufnr, mode or "n", lhs, rhs .. "<cr>", { silent = true, noremap = true })
    end

    bind("äp", '<cmd>lua require("ndreas.lsp.ext").goto_prev_error()')
    bind("än", '<cmd>lua require("ndreas.lsp.ext").goto_next_error()')
    bind("Ä", "<cmd>lua vim.diagnostic.open_float()")
    bind("<leader>ä", '<cmd>lua require("fzf-lua").diagnostics_document()')
    bind("<leader>Ä", '<cmd>lua require("fzf-lua").diagnostics_workspace()')

    local cap = client.server_capabilities
    if cap == nil then
      return
    end

    if cap.definitionProvider then
      bind("gd", "<cmd>lua vim.lsp.buf.definition()")
      if cap.declarationProvider then
        bind("gD", "<cmd>lua vim.lsp.buf.declaration()")
      end
    elseif cap.declarationProvider then
      bind("gd", "<cmd>lua vim.lsp.buf.declaration()")
    end

    if cap.hoverProvider then
      bind("K", "<cmd>lua vim.lsp.buf.hover()")
    end

    if cap.signatureHelpProvider then
      bind("<c-k>", "<cmd>lua vim.lsp.buf.signature_help()")
    end

    if cap.renameProvider then
      bind("<leader>r", "<cmd>lua vim.lsp.buf.rename()")
    end

    if cap.referencesProvider then
      bind("gr", "<cmd>lua vim.lsp.buf.references()")
      bind("<space>li", "<cmd>lua vim.lsp.buf.incoming_calls()")
      bind("<space>lo", "<cmd>lua vim.lsp.buf.outgoing_calls()")
    end

    if cap.documentSymbolProvider then
      require("nvim-navic").attach(client, bufnr)
      bind("<space>j", '<cmd>lua require("fzf-lua").lsp_document_symbols()')
    end
    if cap.workspaceSymbolProvider then
      bind("<space>J", '<cmd>lua require("fzf-lua").lsp_workspace_symbols()')
    end

    if cap.codeActionProvider then
      bind("<leader>a", '<cmd>lua require("fzf-lua").lsp_code_actions()')
      -- bind("<leader>a", ':<c-u>lua require("fzf-lua").lsp_code_actions()', "v")
      bind("<leader>f", '<cmd>lua require("ndreas.lsp.ext").fix_buffer()')
    end

    if cap.documentFormattingProvider and not vim.bo[bufnr].readonly then
      lspformat.on_attach(client, bufnr)
    end

    if cap.documentHighlightProvider then
      local group = augroup(string.format("LSPHighlighting:%d", bufnr), {
        clear = true,
      })
      autocmd({ "CursorHold", "CursorHoldI" }, {
        group = group,
        buffer = bufnr,
        callback = function()
          vim.lsp.buf.document_highlight()
        end,
      })
      autocmd("CursorMoved", {
        group = group,
        buffer = bufnr,
        callback = function()
          vim.lsp.buf.clear_references()
        end,
      })
    end

    if cap.inlayHintProvider then
      vim.lsp.inlay_hint.enable(true, { bufnr = bufnr })
    end
  end,
})

-- Configure diagnostics
require("lsp_lines").setup()

vim.diagnostic.config({
  update_in_insert = false,
  virtual_text = false,
  virtual_lines = { only_current_line = true },
  signs = {
    text = {
      [vim.diagnostic.severity.ERROR] = "▐",
      [vim.diagnostic.severity.WARN] = "▐",
      [vim.diagnostic.severity.INFO] = "▐",
      [vim.diagnostic.severity.HINT] = "▐",
    },
  },
})

-- Configure hover
vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
  border = "rounded",
})

-- Configure formatting
lspformat.setup({
  go = {
    order = { "gopls", "efm" },
  },
  css = { order = { "efm" } },
  graphql = { order = { "efm" } },
  html = { order = { "efm" } },
  javascript = { order = { "efm" } },
  javascriptreact = { order = { "efm" } },
  json = { order = { "efm" } },
  jsx = { order = { "efm" } },
  less = { order = { "efm" } },
  markdown = { order = { "efm" } },
  scss = { order = { "efm" } },
  tsx = { order = { "efm" } },
  typescript = { order = { "efm" } },
  typescriptreact = { order = { "efm" } },
  vue = { order = { "efm" } },
  yaml = { order = { "efm" } },
})

local capabilities = require("blink.cmp").get_lsp_capabilities()

lspconfig.efm.setup({
  capabilities = capabilities,
  root_dir = function(fname)
    return lspconfigutil.root_pattern(".git")(fname) or lspconfigutil.path.dirname(fname)
  end,
  cmd = {
    "efm-langserver",
    "-q",
    "-c",
    home .. "/.config/nvim/efm-langserver.yaml",
  },
  filetypes = {
    "css",
    "go",
    "graphql",
    "html",
    "javascript",
    "javascriptreact",
    "json",
    "jsx",
    "less",
    "lua",
    "markdown",
    "nix",
    "scss",
    "sh",
    "terraform",
    "tsx",
    "typescript",
    "typescriptreact",
    "vue",
    "yaml",
  },
})

local function setup(lsp, cmd, args)
  args = args or {
    capabilities = capabilities,
  }
  if vim.fn.executable(cmd) then
    lspconfig[lsp].setup(args)
  end
end

setup("cssls", "vscode-css-language-server")
setup("dockerls", "docker-langserver")
setup("jsonls", "vscode-json-language-server")
setup("pylsp", "pylsp")
setup("rust_analyzer", "rust-analyzer")
setup("sqlls", "sql-language-server")
setup("terraformls", "terraform-ls")
setup("ts_ls", "typescript-language-server")
setup("yamlls", "yaml-language-server", {
  settings = {
    yaml = {
      keyOrdering = false,
    },
  },
})

setup("gopls", "gopls", {
  capabilities = capabilities,
  cmd = { "gopls", "serve" },
  on_new_config = function(config, root)
    local prefix = os.getenv("GOPLS_LOCAL_PREFIX")
    if not prefix then
      return config
    end
    config.settings.gopls["local"] = prefix .. vim.fs.basename(root)
    return config
  end,
  settings = {
    gopls = {
      gofumpt = true,
      hints = {
        compositeLiteralFields = true,
        parameterNames = true,
      },
      buildFlags = (function()
        local bf = os.getenv("GOPLS_BUILDFLAGS")
        if bf then
          return vim.split(bf, " ")
        end

        return {}
      end)(),
    },
  },
})

setup("golangci_lint_ls", "golangci-lint-langserver", {
  capabilities = capabilities,
  init_options = {
    command = {
      "golangci-lint",
      "run",
      "--enable",
      "errcheck,errorlint,gocritic,gofumpt,gosimple,govet,ineffassign,staticcheck,unconvert,unparam,unused",
      "--out-format",
      "json",
    },
  },
})

if vim.fn.executable("lua-language-server") then
  local luapath = vim.split(package.path, ";")
  table.insert(luapath, "lua/?.lua")
  table.insert(luapath, "lua/?/init.lua")

  local lualib = {}

  local add_lualib = function(s)
    for _, p in pairs(vim.fn.expand(s, false, true)) do
      p = vim.uv.fs_realpath(p)
      if p then
        lualib[p] = true
      end
    end
  end

  add_lualib("$VIMRUNTIME")
  add_lualib("~/.config/nvim")
  add_lualib("~/.local/share/nvim/site/pack/paq/opt/*")
  add_lualib("~/.local/share/nvim/site/pack/paq/start/*")

  lspconfig.lua_ls.setup({
    capabilities = capabilities,
    cmd = { "lua-language-server" },
    on_new_config = function(config, root)
      local libs = vim.tbl_deep_extend("force", {}, lualib)
      libs[root] = nil
      config.settings.Lua.workspace.library = libs
      return config
    end,
    settings = {
      Lua = {
        runtime = { version = "LuaJIT" },
        completion = { callSnippet = "Both" },
        diagnostics = { globals = { "vim" } },
        hint = { enable = true, arrayIndex = "Disable" },
        workspace = {
          library = lualib,
          maxpreload = 2000,
          preloadFileSize = 50000,
        },
        telemetry = { enable = false },
      },
    },
  })
end
