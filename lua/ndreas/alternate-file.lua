local vim = vim
local api = vim.api

local handlers = {
  go = function(fname)
    if fname:match("_test.go$") then
      return fname:gsub("_test.go$", ".go")
    else
      return fname:gsub(".go$", "_test.go")
    end
  end,
}

return function()
  local h = handlers[vim.bo.ft]

  if h ~= nil then
    local fname = h(api.nvim_buf_get_name(0))

    local bufs = api.nvim_tabpage_list_wins(0)

    for _, win in pairs(bufs) do
      local buf = api.nvim_win_get_buf(win)
      local bufname = api.nvim_buf_get_name(buf)

      if fname == bufname then
        api.nvim_set_current_win(win)
        return
      end
    end

    vim.cmd("sp " .. fname)
  end
end
