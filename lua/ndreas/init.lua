require("lazy").setup({
  -- UI
  "kyazdani42/nvim-web-devicons",
  {
    "nvim-lualine/lualine.nvim",
    config = function()
      require("ndreas/statusline")
    end,
  },

  "stevearc/dressing.nvim",
  {
    "ibhagwan/fzf-lua",
    config = function()
      local fzf = require("fzf-lua")
      local actions = require("fzf-lua/actions")

      local file_split_or_qf = function(selected, opts)
        if #selected > 1 then
          actions.file_sel_to_qf(selected, opts)
        else
          actions.file_split(selected, opts)
        end
      end

      fzf.setup({
        width = 0.95,
        git = {
          files = {
            cmd = "git ls-files --exclude-standard --cached --others",
            actions = { default = file_split_or_qf },
          },
        },
        grep = {
          actions = { default = file_split_or_qf },
        },
        hls = {
          header_text = "Constant",
          header_bind = "Keyword",
          buf_name = "Directory",
          buf_nr = "Number",
          buf_linenr = "Normal",
          buf_flag_cur = "Statement",
          buf_flag_alt = "Conditional",
        },
      })

      local b = vim.keymap.set
      local o = { silent = true }
      b("n", "<CR>", fzf.git_files, o)
      b("n", "<leader>b", fzf.buffers, o)
      b("v", "<leader>g", fzf.grep_visual, o)
      b("n", "<leader>h", fzf.help_tags, o)
      b("n", "<leader>q", fzf.quickfix, o)

      b("n", "<leader>g", function()
        local opts = {}
        local git_root = fzf.path.git_root(opts, true)
        if git_root then
          opts["cwd"] = git_root
        end
        fzf.live_grep(opts)
      end, o)
    end,
  },

  -- Editing
  "editorconfig/editorconfig-vim",
  { "windwp/nvim-autopairs", config = true },
  {
    "norcalli/nvim-colorizer.lua",
    opts = {
      css = { css = true },
      less = { css = true },
      sass = { css = true },
      scss = { css = true },
    },
  },
  "tversteeg/registers.nvim",

  -- LSP
  {
    "neovim/nvim-lspconfig",
    event = "BufReadPre",
    dependencies = {
      "ray-x/lsp_signature.nvim",
      "nanotee/sqls.nvim",
      "https://git.sr.ht/~whynothugo/lsp_lines.nvim",
      "lukas-reineke/lsp-format.nvim",
    },
    config = function()
      require("ndreas/lsp")
      require("lsp_signature").on_attach({
        fix_pos = true,
        hint_enable = false,
        handler_opts = { border = "rounded" },
      })
    end,
  },

  {
    "ldelossa/litee-calltree.nvim",
    dependencies = { "ldelossa/litee.nvim" },
    config = function()
      require("litee.lib").setup({})
      require("litee.calltree").setup({
        hide_cursor = false,
        on_open = "panel",
      })
    end,
  },

  "SmiteshP/nvim-navic",

  -- Completion
  {
    "saghen/blink.cmp",
    lazy = false,
    build = "nix run .#build-plugin",
    dependencies = {
      "dcampos/nvim-snippy",
      "moyiz/blink-emoji.nvim",
    },
    opts = {
      enabled = function()
        local bt = vim.bo.buftype
        if bt == "prompt" then
          return false
        end
        if bt == "nofile" and vim.api.nvim_buf_get_name(0) == "" then
          return false
        end
        return true
      end,
      keymap = {
        ["<Tab>"] = {
          function()
            local s = require("snippy")
            if s.can_expand() then
              vim.schedule(s.expand)
              return true
            end
          end,
          "select_next",
          function()
            local s = require("snippy")
            if s.can_jump(1) then
              vim.schedule(s.next)
              return true
            end
          end,
          "snippet_forward",
          "fallback",
        },
        ["<S-Tab>"] = {
          "select_prev",
          function()
            local s = require("snippy")
            if s.can_jump(1) then
              vim.schedule(s.next)
              return true
            end
          end,
          "snippet_backward",
          "fallback",
        },
        ["<CR>"] = { "select_and_accept", "fallback" },
        ["<C-p>"] = { "select_prev", "fallback" },
        ["<C-n>"] = { "select_next", "fallback" },
        ["<Up>"] = { "select_prev", "fallback" },
        ["<Down>"] = { "select_next", "fallback" },
      },
      completion = {
        list = {
          selection = {
            preselect = false,
            auto_insert = true,
          },
        },
        documentation = {
          auto_show = true,
          auto_show_delay_ms = 100,
        },
        trigger = {
          show_on_insert_on_trigger_character = true,
        },
      },
      sources = {
        default = { "lsp", "path", "buffer", "emoji" },
        providers = {
          emoji = {
            module = "blink-emoji",
            name = "emoji",
            score_offset = 15,
            opts = { insert = true },
          },
        },
      },
      cmdline = {
        enabled = true,
        completion = {
          list = {
            selection = { preselect = false },
          },
          menu = { auto_show = true },
        },
        keymap = {
          ["<Tab>"] = { "select_next", "fallback" },
          ["<S-Tab>"] = { "select_prev", "fallback" },
        },
      },
    },
  },

  -- AI
  {
    "olimorris/codecompanion.nvim",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-treesitter/nvim-treesitter",
      "saghen/blink.cmp",
    },
    cmd = {
      "CodeCompanion",
      "CodeCompanionActions",
      "CodeCompanionChat",
      "CodeCompanionCmd",
    },
    init = function()
      vim.api.nvim_set_keymap("n", "<leader>c", "<cmd>CodeCompanionActions<cr>", { silent = true, noremap = true })
      vim.api.nvim_set_keymap("v", "<leader>c", "<cmd>CodeCompanionActions<cr>", { silent = true, noremap = true })
    end,
    config = function()
      local adapter = "ollama"
      if os.getenv("ANTHROPIC_API_KEY") then
        adapter = "anthropic"
      end
      if os.getenv("GEMINI_API_KEY") then
        adapter = "gemini"
      end
      require("codecompanion").setup({
        strategies = {
          chat = { adapter = adapter },
          inline = { adapter = adapter },
        },
        adapters = {
          ollama = function()
            return require("codecompanion.adapters").extend("ollama", {
              env = { url = "http://10.42.42.8:11434" },
            })
          end,
        },
        display = {
          chat = {
            show_settings = true,
            window = {
              layout = "horizontal",
              height = 0.5,
            },
          },
        },
      })
    end,
  },

  -- tree-sitter
  {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdateSync",
    dependencies = {
      "nvim-treesitter/nvim-treesitter-textobjects",
      "RRethy/nvim-treesitter-textsubjects",
      "RRethy/nvim-treesitter-endwise",
      "Wansmer/treesj",
    },
    config = function()
      require("ndreas/treesitter")
    end,
  },

  {
    "lukas-reineke/indent-blankline.nvim",
    main = "ibl",
    opts = {
      indent = {
        char = { "|", "¦", "┆", "┊" },
      },
      scope = {
        show_start = false,
      },
    },
  },

  -- debugging
  {
    "mfussenegger/nvim-dap",
    config = function()
      require("ndreas/dap")
    end,
  },
  { "rcarriga/nvim-dap-ui",  lazy = true },

  -- git
  "nvim-lua/plenary.nvim",
  {
    "lewis6991/gitsigns.nvim",
    opts = {
      signs = {
        add = { text = "│" },
        change = { text = "│" },
        delete = { text = "_" },
        topdelete = { text = "‾" },
        changedelete = { text = "┴" },
        untracked = { text = "┆" },
      },
      signs_staged = {
        add = { text = "│" },
        change = { text = "│" },
        delete = { text = "_" },
        topdelete = { text = "‾" },
        changedelete = { text = "┴" },
        untracked = { text = "┆" },
      },
      numhl = false,
      linehl = false,
      preview_config = {
        border = "rounded",
      },
      on_attach = function(bufnr)
        local gs = package.loaded.gitsigns
        local opts = { buffer = bufnr }
        vim.keymap.set("n", "<space>gn", gs.next_hunk, opts)
        vim.keymap.set("n", "<space>gp", gs.prev_hunk, opts)
        vim.keymap.set("n", "<space>gd", gs.preview_hunk, opts)
      end,
    },
  },

  {
    "rhysd/git-messenger.vim",
    init = function()
      vim.g.git_messenger_no_default_mappings = true
      vim.g.git_messenger_floating_win_opts = { border = "rounded" }
    end,
    config = function()
      vim.keymap.set("n", "<space>gm", "<plug>(git-messenger)")
    end,
  },

  -- input

  "ndreas/vim-indent-object",

  {
    "machakann/vim-sandwich",
    init = function()
      vim.g.sandwich_no_default_key_mappings = 1
      vim.g.operator_sandwich_no_default_key_mappings = 1
    end,
    config = function()
      vim.keymap.set(
        "n",
        "ds",
        "<Plug>(operator-sandwich-delete)<Plug>(operator-sandwich-release-count)<Plug>(textobj-sandwich-query-a)"
      )
      vim.keymap.set(
        "n",
        "dss",
        "<Plug>(operator-sandwich-delete)<Plug>(operator-sandwich-release-count)<Plug>(textobj-sandwich-auto-a)"
      )
      vim.keymap.set(
        "n",
        "cs",
        "<Plug>(operator-sandwich-replace)<Plug>(operator-sandwich-release-count)<Plug>(textobj-sandwich-query-a)"
      )
      vim.keymap.set(
        "n",
        "css",
        "<Plug>(operator-sandwich-replace)<Plug>(operator-sandwich-release-count)<Plug>(textobj-sandwich-auto-a)"
      )
      vim.keymap.set("x", "S", "<Plug>(operator-sandwich-add)")
    end,
  },

  { "windwp/nvim-ts-autotag",    config = true },
  { "numToStr/Comment.nvim",     config = true },
  { "nmac427/guess-indent.nvim", config = true },

  -- filetypes
  "kmonad/kmonad-vim",
  "elkowar/yuck.vim",
  "dag/vim-fish",
  "hashivim/vim-terraform",
  "NoahTheDuke/vim-just",
})

require("ndreas/session")

vim.keymap.set("n", "ga", function()
  require("ndreas/alternate-file")()
end)
