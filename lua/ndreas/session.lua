local vim = vim
local fn = vim.fn

local function find_root()
  local root = vim.trim(fn.system("git rev-parse --show-toplevel"))
  if vim.v.shell_error ~= 0 then
    root = fn.expand("%:p:h")
  end
  return root
end

local M = {}

M.save_session_and_exit = function()
  vim.cmd("wa!")
  local root = find_root()
  vim.cmd({
    cmd = "mksession",
    args = { root .. "/Session.vim" },
  })
  vim.cmd("xa")
end

vim.api.nvim_create_user_command("XSession", M.save_session_and_exit, {})

return M
