local vim = vim
local highlight = vim.api.nvim_set_hl

local colors = {
  base = "#282c34",
  bg = "#20232a",
  fg = "#abb2bf",

  base_l2 = "#2c313a",
  base_l15 = "#434956",
  fg_l10 = "#b3bac6",
  fg_d65 = "#373d49",
  fg_d65_l20 = "#586274",

  dark_grey = "#353b45",
  grey = "#565c64",
  red = "#e06c75",
  green = "#98c379",
  blue = "#61afef",
  yellow = "#e5c07b",
  orange = "#d19a66",
  purple = "#c678dd",
  teal = "#56b6c2",

  blue_ds45 = "#81accf",

  diff = {
    add = "#3c5828",
    change = "#775618",
    delete = "#6d171e",
    text = "#0c4879",
  },
}

local highlights = {
  ColorColumn = { bg = colors.base_l15 },
  Conceal = { fg = colors.blue },
  CurSearch = { fg = colors.base, bg = colors.yellow, bold = true },
  Cursor = { reverse = true },
  lCursor = { link = "Cursor" },
  CursorIM = { link = "Cursor" },
  CursorColumn = { link = "CursorLine" },
  CursorLine = { bg = colors.base },
  Directory = { fg = colors.blue, bold = true },
  DiffAdd = { bg = colors.diff.add },
  DiffChange = { bg = colors.diff.change },
  DiffDelete = { bg = colors.diff.delete, bold = true },
  DiffText = { bg = colors.diff.text },
  EndOfBuffer = { fg = colors.bg },
  -- TermCursor: Cursor in a focused terminal.
  -- TermCursorNC: Cursor in an unfocused terminal.
  ErrorMsg = { fg = colors.red, bold = true },
  VertSplit = { fg = colors.fg_d65 },
  WinSeparator = { link = "VertSplit" },
  Folded = { fg = colors.grey },
  FoldColumn = { link = "Folded" },
  SignColumn = { fg = colors.grey },
  LineNr = { fg = colors.fg_d65 },
  -- LineNrAbove: Line number for when the 'relativenumber' option is set, above the cursor line.
  -- LineNrBelow: Line number for when the 'relativenumber' option is set, below the cursor line.
  CursorLineNr = { fg = colors.fg_d65_l20 },
  -- CursorLineFold: Like FoldColumn when 'cursorline' is set for the cursor line.
  -- CursorLineSign: Like SignColumn when 'cursorline' is set for the cursor line.
  MatchParen = { bg = colors.gray },
  ModeMsg = { fg = colors.green, bold = true },
  MsgArea = { link = "Normal" },
  MsgSeparator = { fg = colors.gray },
  MoreMsg = { link = "ModeMsg" },
  NonText = { link = "Whitespace" },
  Normal = { bg = colors.bg, fg = colors.fg },
  NormalFloat = { bg = colors.bg, fg = colors.fg },
  FloatBorder = { fg = colors.gray },
  FloatTitle = { fg = colors.gray, bold = true },
  NormalNC = { link = "Normal" },
  Pmenu = { bg = colors.base, fg = colors.fg },
  PmenuSel = { bg = colors.green, fg = colors.dark_grey },
  PmenuSbar = { bg = colors.dark_grey },
  PmenuThumb = { bg = colors.blue_ds45 },
  Question = { link = "MoreMsg" },
  QuickFixLine = { bg = colors.dark_grey },
  Search = { bg = colors.yellow, fg = colors.base },
  IncSearch = { bg = colors.orange, fg = colors.base },
  Substitute = { link = "Search" },
  SpecialKey = { fg = colors.grey },
  SpellBad = { sp = colors.red, undercurl = true },
  SpellCap = { sp = colors.blue, undercurl = true },
  SpellLocal = { sp = colors.teal, undercurl = true },
  SpellRare = { sp = colors.teal, undercurl = true },
  StatusLine = { bg = colors.dark_grey, fg = colors.fg },
  StatusLineNC = { bg = colors.base, fg = colors.grey },
  TabLine = { bg = colors.dark_grey, fg = colors.grey },
  TabLineFill = { link = "TabLine" },
  TabLineSel = { bg = colors.bg, fg = colors.fg_l10 },
  Title = { fg = colors.blue },
  Visual = { bg = colors.base_l15 },
  VisualNOS = { bg = colors.base_l15, fg = colors.red },
  WarningMsg = { fg = colors.red, bold = true, italic = true },
  Whitespace = { fg = colors.fg_d65 },
  WildMenu = { bg = colors.green, fg = colors.dark_grey },
  -- Winbar = { fg = theme.ui.fg_dim, bg = "NONE" },
  -- WinbarNC = { fg = theme.ui.fg_dim, bg = config.dimInactive and theme.ui.bg_dim or "NONE" },

  -- SignColumnSB = { link = "SignColumn" },
  -- NormalSB = { link = "Normal" },

  -- debugPC = { bg = theme.diff.delete },
  -- debugBreakpoint = { fg = theme.syn.special1, bg = theme.ui.bg_gutter },

  LspReferenceText = { bg = colors.dark_grey, italic = true },
  LspReferenceRead = { link = "LspReferenceText" },
  LspReferenceWrite = { link = "LspReferenceText" },

  DiagnosticError = { fg = colors.red },
  DiagnosticWarn = { fg = colors.orange },
  DiagnosticInfo = { fg = colors.blue },
  DiagnosticHint = { fg = colors.grey },
  DiagnosticOk = { fg = colors.green },

  DiagnosticFloatingError = { link = "DiagnosticError" },
  DiagnosticFloatingWarn = { link = "DiagnosticWarn" },
  DiagnosticFloatingInfo = { link = "DiagnosticInfo" },
  DiagnosticFloatingHint = { link = "DiagnosticHint" },
  DiagnosticSignError = { link = "DiagnosticError" },
  DiagnosticSignWarn = { link = "DiagnosticWarn" },
  DiagnosticSignInfo = { link = "DiagnosticInfo" },
  DiagnosticSignHint = { link = "DiagnosticHint" },
  DiagnosticVirtualTextError = { link = "DiagnosticError" },
  DiagnosticVirtualTextWarn = { link = "DiagnosticWarn" },
  DiagnosticVirtualTextInfo = { link = "DiagnosticInfo" },
  DiagnosticVirtualTextHint = { link = "DiagnosticHint" },
  DiagnosticUnderlineError = { sp = colors.red, undercurl = true },
  DiagnosticUnderlineWarn = { sp = colors.orange, undercurl = true },
  DiagnosticUnderlineInfo = { sp = colors.blue, undercurl = true },
  DiagnosticUnderlineHint = { sp = colors.grey, undercurl = true },

  LspSignatureActiveParameter = { italic = true },
  LspCodeLens = { fg = colors.grey },
  LspCodeLensSeparator = { fg = colors.dark_grey },
  LspInlayHint = { fg = colors.grey, italic = true },

  Comment = { fg = colors.grey, italic = true },

  Constant = { fg = colors.orange },
  String = { fg = colors.green },
  Character = { link = "String" },
  Number = { fg = colors.orange },
  Boolean = { fg = colors.orange, bold = true },
  Float = { link = "Number" },

  Identifier = { fg = colors.red },
  Function = { fg = colors.blue, bold = true },

  Statement = { fg = colors.red, bold = true },
  Conditional = { fg = colors.purple },
  Repeat = { fg = colors.yellow },
  Label = { fg = colors.purple },
  Operator = { fg = colors.fg },
  Keyword = { fg = colors.purple },
  Exception = { fg = colors.red, italic = true },

  PreProc = { fg = colors.yellow },
  PreCondit = { link = "PreProc" },
  Include = { fg = colors.blue },
  Define = { fg = colors.purple },
  Macro = { fg = colors.red },

  Type = { fg = colors.orange },
  Typedef = { link = "Type" },
  StorageClass = { fg = colors.orange },
  Structure = { fg = colors.purple },

  Special = { fg = colors.teal },
  SpecialChar = { fg = colors.red, italic = true },
  Tag = { fg = colors.yellow },
  Delimiter = { fg = colors.fg },
  SpecialComment = { link = "Special" },
  Debug = { fg = colors.red },

  Underlined = { underline = true },
  Bold = { bold = true },
  Italic = { italic = true },

  Ignore = { fg = colors.bg },

  Error = { sp = colors.red, undercurl = true },

  Todo = { bg = colors.dark_grey, fg = colors.yellow },

  qfLineNr = { link = "lineNr" },
  qfFileName = { link = "Directory" },

  ["@comment"] = { fg = colors.grey, italic = true },
  ["@comment.todo"] = { bg = colors.dark_grey, fg = colors.yellow },
  ["@comment.note"] = { fg = colors.blue, italic = true },
  ["@comment.warning"] = { fg = colors.yellow, italic = true },
  ["@comment.danger"] = { fg = colors.red, italic = true },
  ["@error"] = { sp = colors.red, undercurl = true },
  -- @none
  ["@operator"] = { link = "Operator" },

  ["@punctuation.delimiter"] = { fg = colors.fg },
  ["@punctuation.bracket"] = { fg = colors.fg },

  ["@string"] = { fg = colors.green },
  ["@string.regexp"] = { fg = colors.purple },
  ["@string.escape"] = { fg = colors.purple },
  ["@string.special.url"] = { underline = true },

  ["@character"] = { link = "@string" },
  ["@character.special"] = { fg = colors.teal },

  ["@boolean"] = { fg = colors.orange, bold = true },
  ["@number"] = { fg = colors.orange },
  ["@number.float"] = { link = "@number" },

  ["@function"] = { fg = colors.blue, bold = true },
  ["@function.builtin"] = { fg = colors.blue, italic = true },
  ["@function.call"] = { fg = colors.blue },
  ["@function.macro"] = { fg = colors.red },
  ["@function.method"] = { link = "@function" },
  ["@function.method.call"] = { link = "@function.call" },

  ["@constructor"] = { fg = colors.teal },

  ["@keyword.function"] = { fg = colors.purple, italic = true },
  ["@keyword.operator"] = { fg = colors.fg },
  ["@keyword.return"] = { fg = colors.red, italic = true },
  ["@keyword.directive"] = { fg = colors.yellow },
  ["@keyword.directive.define"] = { fg = colors.purple },
  ["@keyword.storage"] = { fg = colors.orange },
  ["@keyword.conditional"] = { fg = colors.purple },
  ["@keyword.conditional.ternary"] = { fg = colors.purple },
  ["@keyword.debug"] = { fg = colors.red },
  ["@keyword.repeat"] = { fg = colors.yellow },
  ["@keyword.include"] = { fg = colors.blue },
  ["@keyword.exception"] = { fg = colors.red, italic = true },

  ["@label"] = { fg = colors.purple },

  ["@type"] = { fg = colors.orange },
  ["@type.builtin"] = { fg = colors.orange, italic = true },
  ["@type.definition"] = { link = "@type" },
  ["@type.qualifier"] = { link = "@type" },

  ["@structure"] = { fg = colors.purple },

  ["@attribute"] = { fg = colors.red },
  ["@property"] = { fg = colors.fg },

  ["@variable"] = { fg = colors.red },
  ["@variable.member"] = { fg = colors.red },
  ["@variable.parameter"] = { fg = colors.teal },
  ["@variable.builtin"] = { fg = colors.red, italic = true },

  ["@constant"] = { fg = colors.orange },
  ["@constant.builtin"] = { fg = colors.orange, italic = true },
  ["@constant.macro"] = { fg = colors.red },

  ["@module"] = { fg = colors.teal },
  ["@symbol"] = { fg = colors.teal },

  ["@text"] = {},
  ["@markup.strong"] = { bold = true },
  ["@markup.emphasis"] = { italic = true },
  ["@markup.underline"] = { underline = true },
  ["@markup.strikethrough"] = { strikethrough = true },
  ["@markup.heading"] = { fg = colors.blue },
  ["@markup.raw"] = { fg = colors.green },
  ["@markup.quote"] = { fg = colors.green },
  ["@markup.uri"] = { underline = true },
  ["@markup.math"] = { fg = colors.yellow },
  ["@markup.environment"] = { fg = colors.teal },
  ["@markup.environment.name"] = { fg = colors.teal },
  ["@markup.link"] = { fg = colors.purple },
  ["@markup.link.url"] = { fg = colors.blue },
  ["@markup.link.label"] = { fg = colors.red, italic = true },
  ["@markup.list"] = { fg = colors.purple },

  ["@diff.add"] = { bg = colors.diff.add },
  ["@diff.delete"] = { bg = colors.diff.delete, bold = true },

  ["@tag"] = { fg = colors.red },
  ["@tag.attribute"] = { fg = colors.orange },
  ["@tag.delimiter"] = { fg = colors.yellow },

  -- filetypes
  ["@property.yaml"] = { fg = colors.red },

  -- Gitsigns

  GitSignsAdd = { fg = colors.green },
  GitSignsChange = { fg = colors.yellow },
  GitSignsDelete = { fg = colors.red },

  -- indentline

  IblIndent = { fg = colors.base_l2 },
  IblScope = { fg = colors.teal },

  -- dap

  DapSignBreakpoint = { fg = colors.red },
  DapSignBreakpointCondition = { fg = colors.orange },
  DapSignBreakpointRejected = { fg = colors.grey },
  DapSignLogPoint = { fg = colors.blue },
  DapSignBreakpointStopped = { fg = colors.green },
}

vim.cmd("hi clear")

for hl, spec in pairs(highlights) do
  highlight(0, hl, spec)
end
