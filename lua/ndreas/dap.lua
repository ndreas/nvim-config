local vim = vim
local api = vim.api
local dap = require("dap")

for _, t in pairs({
  { name = "Breakpoint", sign = "⊙" },
  { name = "BreakpointCondition", sign = "⊙" },
  { name = "BreakpointRejected", sign = "⊗" },
  { name = "LogPoint", sign = "⊙" },
  { name = "Stopped", sign = "►" },
}) do
  vim.fn.sign_define("Dap" .. t.name, {
    text = t.sign,
    texthl = "DapSign" .. t.name,
    linehl = "",
    numhl = "",
  })
end

vim.keymap.set("n", "<leader>db", function()
  dap.toggle_breakpoint()
end)
vim.keymap.set("n", "<leader>dc", function()
  dap.set_breakpoint(vim.fn.input("Breakpoint condition msg: "))
end)
vim.keymap.set("n", "<leader>dl", function()
  dap.set_breakpoint(nil, nil, vim.fn.input("Log point msg: "))
end)

api.nvim_create_user_command("Debug", function()
  local ok, dapui = pcall(require, "dapui")
  if ok then
    dapui.open()
    dap.continue()
    return
  end

  for _, ft in pairs({ "go" }) do
    require("ndreas/dap/" .. ft).setup()
  end

  dapui = require("dapui")
  dapui.setup()
  dapui.open()
  dap.continue()
end, {})
