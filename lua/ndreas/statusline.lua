local vim = vim
local mode = vim.fn.mode
local nvim_command = vim.api.nvim_command

local lualine = require("lualine")

local colors = {
  red = "#e06c75",
  green = "#98c379",
  blue = "#61afef",
  orange = "#d19a66",

  xgray1 = "#282c34",
  xgray2 = "#353b45",
  xgray5 = "#545862",
  xgray6 = "#565c64",
  xgray7 = "#6f737b",
  xgray8 = "#abb2bf",
}

vim.o.showmode = false

local bg = colors.xgray2
local fg = colors.xgray6
local active_highlight = { bg = bg, fg = fg }
local inactive_highlight = { fg = colors.xgray6, gui = "underline" }

local mode_map = {
  ["__"] = { icon = "󰭯", color = colors.xgray8 },
  ["n"] = { icon = "󰬕", color = colors.xgray8 },
  ["i"] = { icon = "󰬐", color = colors.blue },
  ["v"] = { icon = "󰬝", color = colors.orange },
  ["V"] = { icon = "󰬝", color = colors.orange },
  [""] = { icon = "󰬝", color = colors.orange },
  ["R"] = { icon = "󰬙", color = colors.red },
  ["r"] = { icon = "󰬙", color = colors.red },
  ["Rv"] = { icon = "󰬙", color = colors.red },
  ["c"] = { icon = "󰬊", color = colors.green },
  ["t"] = { icon = "󰬛", color = colors.xgray8 },
}

local navic = require("nvim-navic")
navic.setup({
  separator = "󰅂 ",
})

lualine.setup({
  options = {
    component_separators = "",
    section_separators = "",
    theme = {
      normal = {
        a = active_highlight,
        b = active_highlight,
        c = active_highlight,
        x = active_highlight,
        y = active_highlight,
        z = active_highlight,
      },
      inactive = {
        a = inactive_highlight,
        b = inactive_highlight,
        c = inactive_highlight,
        x = inactive_highlight,
        y = inactive_highlight,
        z = inactive_highlight,
      },
    },
  },
  sections = {
    lualine_a = {
      {
        function()
          local m = mode_map[mode()]
          nvim_command("hi! LualineMode guifg=" .. m.color .. " guibg=" .. bg)
          local paste = ""
          if vim.o.paste then
            paste = " 󰰚 "
          end
          return "▌" .. m.icon .. paste
        end,
        color = "LualineMode",
        padding = { right = 1 },
      },
    },
    lualine_b = {
      {
        "diagnostics",
        sources = { "nvim_diagnostic" },
        sections = { "error", "warn" },
        symbols = { error = "󰅙 ", warn = "󰀦 " },
      },
    },
    lualine_c = {
      {
        function()
          if not navic.is_available() then
            return ""
          end

          local loc = navic.get_location()

          if loc == "" then
            return ""
          end

          return "󰅂 " .. loc
        end,
        color = { fg = colors.xgray7 },
      },
    },
    lualine_x = {},
    lualine_y = {
      { "filetype", icon_only = true },
      {
        "filename",
        symbols = { modified = "", readonly = "" },
        path = 0,
        color = { fg = colors.xgray7 },
        padding = { left = 0 },
      },
    },
    lualine_z = {
      { "location", padding = { left = 1 } },
      {
        function()
          return "▐"
        end,
        color = "LualineMode",
        padding = 0,
      },
    },
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = {},
    lualine_x = {},
    lualine_y = {
      {
        "filename",
        symbols = { modified = "", readonly = "" },
        path = 1,
      },
    },
    lualine_z = {},
  },
})
