local vim = vim
local lsp = vim.lsp
local diagnostic = vim.diagnostic
local ERROR = diagnostic.severity.ERROR
local WARN = diagnostic.severity.WARN

local M = {}

M.fix_buffer = function()
  local params = lsp.util.make_range_params()
  params.context = { diagnostics = {}, only = { "source.organizeImports" } }

  local responses = lsp.buf_request_sync(0, "textDocument/codeAction", params)

  if not responses or vim.tbl_isempty(responses) then
    return
  end

  for client_id, response in pairs(responses) do
    for _, result in pairs(response.result or {}) do
      if result.edit then
        lsp.util.apply_workspace_edit(result.edit, vim.lsp.get_client_by_id(client_id).offset_encoding)
      else
        lsp.buf.execute_command(result.command)
      end
    end
  end
end

M.goto_prev_error = function()
  local sev = WARN
  if next(diagnostic.get(0, { severity = ERROR })) then
    sev = ERROR
  end
  return diagnostic.goto_prev({
    severity = sev,
  })
end

M.goto_next_error = function()
  local sev = WARN
  if next(diagnostic.get(0, { severity = ERROR })) then
    sev = ERROR
  end
  return diagnostic.goto_next({
    severity = sev,
  })
end

return M
