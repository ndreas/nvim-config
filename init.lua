local vim = vim
local api = vim.api

--
-- Settings
--

vim.g.mapleader = ","

-- Appearance
vim.o.title = true
vim.o.titlestring = "nvim %t"
vim.o.list = true

vim.o.listchars = "tab:⇥ ,eol:↴,extends:❯,precedes:❮"
vim.o.showbreak = "↳"
vim.o.breakindent = true
vim.o.breakindentopt = "sbr,list:-1"
vim.o.textwidth = 0
vim.o.wrap = true
vim.o.lazyredraw = true
vim.o.guicursor = "v-c-sm:block,i-ci-ve:ver25,r-cr-o:hor20,r:rCursor,v-ve:vCursor"
vim.o.pumheight = 12

vim.o.background = "dark"
vim.o.termguicolors = true

-- Behaviour
vim.o.backspace = "indent,eol,start"
vim.o.scrolloff = 2
vim.o.updatetime = 300
vim.o.mouse = "n"

-- Whitespace
vim.o.autoindent = true
vim.o.expandtab = true
vim.o.shiftround = true
vim.o.shiftwidth = 4
vim.o.softtabstop = 4
vim.o.tabstop = 4

-- Buffers
vim.o.autoread = true
vim.o.autowrite = true
vim.o.autochdir = true
vim.o.hidden = true

-- Folding
vim.o.foldenable = false -- toggle with zi
vim.o.foldlevel = 0
vim.o.foldmethod = "indent"

-- Search
vim.o.gdefault = true
vim.o.hlsearch = true
vim.o.ignorecase = true
vim.o.incsearch = true
vim.o.showmatch = true
vim.o.smartcase = true
vim.o.wrapscan = false

vim.o.inccommand = "nosplit"

-- Status
vim.o.laststatus = 2
vim.o.showcmd = true
vim.o.showmode = true

vim.o.statusline = " %t%m%r › %{expand('%:p:~:h')} %= %{&ft} ‹ %{&fenc} ‹ 0x%B ‹ %l,%c%V ‹ %p%% "

-- Persisting
vim.o.backup = false
vim.o.history = 10000
vim.o.undofile = true

-- Tags
vim.o.tags = "tags;"

-- Completion
vim.o.completeopt = "menu,menuone,noselect"
vim.o.wildmode = "list:longest,full"
vim.o.wildignorecase = true

-- Diffing
vim.o.diffopt = "filler,internal,algorithm:histogram,indent-heuristic"

-- Disable matchparen
vim.g.loaded_matchparen = 1
vim.g.loaded_matchit = 1

-- Syntax/filetype
vim.g.python_highlight_all = 1
vim.g.vimsyn_embed = "lP"

vim.cmd([[
syntax on
filetype plugin on
filetype indent on
]])

--
-- Highlighting
--

vim.cmd("hi! link TrailingWhitespace Error")
api.nvim_create_autocmd({ "Syntax" }, {
  pattern = "*",
  callback = function()
    if not vim.tbl_contains({ "fzf", "cmp_menu", "cmp_docs" }, vim.bo.filetype) then
      vim.cmd("syn match TrailingWhitespace /\\s\\+$/ containedin=ALL")
    end
  end,
})

--
-- Key mappings
--

-- Window
vim.keymap.set("", "ö", "<C-W>")
vim.keymap.set("", "öö", "<C-W>_")
vim.keymap.set("", "öe", "<C-W>=")
vim.keymap.set("", "öf", "<C-W><C-F>")

-- Jumping
vim.keymap.set("", "å", "]")
vim.keymap.set("", "å", "[")

-- Close quickfix windows
vim.keymap.set("n", "<Esc>", "<Esc>:ccl<CR>:lcl<CR>", { silent = true })
-- Hide search highlight
vim.keymap.set("n", "<BS>", "<Esc>:nohlsearch<CR>", { silent = true })
-- Zoom window
vim.keymap.set("n", "<leader>z", "<Esc>:tab split<CR>", { silent = true })

--
-- Autocommands
--

local function autocmd(cmd, pat, excmd)
  api.nvim_create_autocmd(cmd, {
    pattern = pat,
    command = excmd,
  })
end

-- Check for modifications
autocmd("CursorHold", "*", "silent! checktime")
-- Quickfix jumping
autocmd("BufReadPost", "quickfix", "nmap <buffer> <CR> <C-W><CR>")
-- Help file jumping
autocmd("FileType", "help", "nmap <buffer> <CR> g<C-]>")

-- Jump to the lastcursor position
autocmd(
  "BufReadPost",
  "*",
  [[ if line("'\"") > 0 && line("'\"") <= line("$") && &ft !~# 'commit' | exe "normal g'\"" | endif ]]
)

-- Cursorline only in active buffer
autocmd({ "VimEnter", "WinEnter", "BufWinEnter" }, "*", "setlocal cursorline")
autocmd("WinLeave", "*", "setlocal nocursorline")

-- Filetype settings
local function ftcmd(pat, callback)
  api.nvim_create_autocmd("FileType", {
    pattern = pat,
    callback = callback,
  })
end

ftcmd("go", function()
  vim.bo.expandtab = false
  vim.bo.tabstop = 2
  vim.bo.shiftwidth = 2
  vim.opt_local.listchars = "tab:⬞ ,eol:↴,extends:❯,precedes:❮"
end)

ftcmd("markdown", function()
  vim.bo.textwidth = 80
end)

ftcmd({ "vim", "json", "html", "yaml", "nix", "lua", "sh", "zsh", "just", "javascript", "typescript" }, function()
  vim.bo.tabstop = 2
  vim.bo.softtabstop = 2
  vim.bo.shiftwidth = 2
end)

ftcmd("yaml", function()
  vim.api.nvim_buf_create_user_command(0, "KubeSecretDecode", function()
    vim.cmd([[%!yq ".data = (.data |map_values(@base64d))"]])
  end, {})
  vim.api.nvim_buf_create_user_command(0, "KubeSecretDecodeList", function()
    vim.cmd([[%!yq ".items = (.items | map(.data = (.data |map_values(@base64d))))"]])
  end, {})
  vim.api.nvim_buf_create_user_command(0, "KubeSecretEncode", function()
    vim.cmd([[%!yq ".data = (.data |map_values(@base64))"]])
  end, {})
  vim.api.nvim_buf_create_user_command(0, "KubeSecretEncodeList", function()
    vim.cmd([[%!yq ".items = (.items | map(.data = (.data |map_values(@base64))))"]])
  end, {})
end)

if not vim.uv.fs_stat(vim.fn.stdpath("config") .. "/lua/ndreas/init.lua") then
  return
end

require("ndreas/theme")

-- lazy.nvim
local lazy_path = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

if not vim.uv.fs_stat(lazy_path) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable",
    lazy_path,
  })
end

vim.opt.rtp:prepend(lazy_path)

require("ndreas")
