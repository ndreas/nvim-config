#!/bin/sh

force=$1

cd "$HOME" || exit

symlink_file() {
  file=$1

  if [ ! -e "$file" ]; then
    echo "$HOME/$file not found, skipping"
    return
  fi

  target=$2

  if [ -e "$target" ] && [ "$force" != "-f" ]; then
    echo "$HOME/$target exists, skipping, use -f to overwrite"
    return
  fi

  echo "Linking $HOME/$target to $HOME/$file"
  ln -sf "$file" "$target"
}

symlink_file .config/nvim          .vim
symlink_file .config/nvim/init.vim .vimrc

mkdir -p .vim-tmp
